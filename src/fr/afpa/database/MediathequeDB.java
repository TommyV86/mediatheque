package fr.afpa.database;

import fr.afpa.utilitary.ConstantUtilitary;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MediathequeDB {
    private static MediathequeDB instance;
    private final ConstantUtilitary constants = ConstantUtilitary.getInstance();

    public static MediathequeDB getInstance() {
        if (instance == null) {
            instance = new MediathequeDB();
        }
        return instance;
    }

    public Connection connect() {

        Connection conn = null;
        try {
            conn = DriverManager.getConnection(constants.URL, constants.USER_NAME, constants.PASSWORD);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return conn;
    }
}
