package fr.afpa.controller;

import fr.afpa.model.User;
import fr.afpa.service.implementation.AppViewServiceIMP;
import fr.afpa.service.IAppViewService;

public class AppViewController {
    private final IAppViewService appService = new AppViewServiceIMP();

    private static AppViewController instance;
    public static AppViewController getInstance() {
        if (instance == null) {
            instance = new AppViewController();
        }
        return instance;
    }

    public void getPublicMenu(){
        appService.getMenu();
    }
    public void getUserMenu(User user){
        appService.getUserMenu(user);
    }
}
