package fr.afpa.controller;

import fr.afpa.model.User;

public interface IController<T> {
    void showAll();

    void supprimer();

    void findById();

    void enregistrer();
}
