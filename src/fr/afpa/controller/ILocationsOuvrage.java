package fr.afpa.controller;

import fr.afpa.model.Ouvrage;
import fr.afpa.model.User;

import java.sql.SQLException;

public interface ILocationsOuvrage<T> {
    void findReservations() throws SQLException;
    void updateReservations(Ouvrage ouvrage) throws SQLException;
}
