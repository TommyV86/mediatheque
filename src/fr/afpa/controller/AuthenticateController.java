package fr.afpa.controller;

import fr.afpa.model.User;
import fr.afpa.service.IAuthenticateService;
import fr.afpa.service.implementation.AuthenticateServiceIMP;
import fr.afpa.view.IAppView;
import fr.afpa.view.implementation.AppViewIMP;

import java.sql.SQLException;

public class AuthenticateController {
    private static AuthenticateController instance;
    public static AuthenticateController getInstance() {
        if (instance == null) {
            instance = new AuthenticateController();
        }
        return instance;
    }
    private static final IAppView appView = AppViewIMP.getInstance();
    private final IAuthenticateService authenticateService = AuthenticateServiceIMP.getInstance();


    public void create(User user) throws SQLException {
        authenticateService.create(appView.promptUser(user));
    }

    public void createAdherent(User user) throws SQLException {
        authenticateService.createAdherent(appView.promptMediathecaire(user));
    }

    public User find(User user) throws SQLException {
        return authenticateService.find(appView.promptFindUser(user));
    }

    public User getUser(User user) throws SQLException {
        return authenticateService.getUser(appView.getUser(user));
    }

    public void updateSolde(User user) throws SQLException {
        authenticateService.updateSolde(appView.promptUpdateSolde(user));
    }

    public void updateSoldeReservation(User user) throws SQLException {
        authenticateService.updateSolde(user);
    }

    public void update(User user) throws SQLException {
        authenticateService.update(appView.promptUpdateUser(user));
    }

    public void delete(User user) throws SQLException {
        authenticateService.delete(appView.promptDeleteUser(user));
    }

    public void getDelete(User user) throws SQLException {
        authenticateService.delete(appView.getUser(user));
    }

    public boolean exist(long id) throws SQLException{
        return authenticateService.exist(id);
    }
}
