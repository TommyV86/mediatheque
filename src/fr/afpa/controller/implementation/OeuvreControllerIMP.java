package fr.afpa.controller.implementation;

import fr.afpa.controller.IController;
import fr.afpa.model.Oeuvre;
import fr.afpa.model.User;
import fr.afpa.service.implementation.OeuvreServiceIMP;

import java.sql.SQLException;
import java.text.ParseException;

public class OeuvreControllerIMP implements IController<Oeuvre> {

    private static OeuvreServiceIMP oeuvreService = OeuvreServiceIMP.getInstance();

    private static OeuvreControllerIMP instance;

    public static OeuvreControllerIMP getInstance() {
        if (instance == null) {
            instance = new OeuvreControllerIMP();
        }
        return instance;
    }

    @Override
    public void showAll() {
        try {
            oeuvreService.showAll();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void supprimer(){
        try {
            oeuvreService.supprimer();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void findById(){
        try {
            oeuvreService.findById();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void enregistrer(){
        try {
            oeuvreService.enregistrer();
        } catch (ParseException e) {
            throw new RuntimeException(e);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
