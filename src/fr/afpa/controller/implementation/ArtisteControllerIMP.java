package fr.afpa.controller.implementation;

import fr.afpa.controller.IController;
import fr.afpa.model.Artiste;
import fr.afpa.model.User;
import fr.afpa.service.implementation.ArtisteServiceIMP;

import java.sql.SQLException;

public class ArtisteControllerIMP implements IController<Artiste> {

    private static ArtisteServiceIMP artisteService = ArtisteServiceIMP.getInstance();

    private static ArtisteControllerIMP instance;


    public static ArtisteControllerIMP getInstance() {
        if (instance == null) {
            instance = new ArtisteControllerIMP();
        }
        return instance;
    }

    @Override
    public void showAll() {
        try {
            artisteService.showAll();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void supprimer() {
        try {
            artisteService.supprimer();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void findById() {
        try {
            artisteService.findById();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void enregistrer() {
        try {
            artisteService.enregistrer();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
