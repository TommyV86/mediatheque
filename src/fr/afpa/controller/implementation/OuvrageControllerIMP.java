package fr.afpa.controller.implementation;

import fr.afpa.controller.IController;
import fr.afpa.controller.ILocationsOuvrage;
import fr.afpa.model.Ouvrage;
import fr.afpa.model.User;
import fr.afpa.service.implementation.OuvrageServiceIMP;
import fr.afpa.view.IAppView;
import fr.afpa.view.implementation.AppViewIMP;

import java.sql.SQLException;
import java.text.ParseException;

public class OuvrageControllerIMP implements IController, ILocationsOuvrage {

    private IAppView appView = AppViewIMP.getInstance();
    private OuvrageServiceIMP ouvrageService = OuvrageServiceIMP.getInstance();

    private static OuvrageControllerIMP instance;

    public static OuvrageControllerIMP getInstance() {
        if (instance == null) {
            instance = new OuvrageControllerIMP();
        }
        return instance;
    }

    @Override
    public void showAll() {
        try {
            ouvrageService.showAll();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void findReservations() throws SQLException{
        ouvrageService.findReservations();
    }

    @Override
    public void updateReservations(Ouvrage ouvrage) throws SQLException{
        ouvrageService.updateReservations(appView.promptUpdateReservations(ouvrage));
    }

    @Override
    public void supprimer() {
        try {
            ouvrageService.supprimer();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void findById() {
        try {
            ouvrageService.findById();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void enregistrer() {
        try {
            ouvrageService.enregistrer();
        } catch (ParseException | SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void reserver(User user){
        try {
            ouvrageService.reserver(user);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void affichageReservationsUtilisateur(User user){
        try {
            ouvrageService.afficherReservationsUtilisateur(user);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void annulerReservation(User user){
        try {
            ouvrageService.annulerReservation(user);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
