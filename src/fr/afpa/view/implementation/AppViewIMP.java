package fr.afpa.view.implementation;

import fr.afpa.model.Ouvrage;

import fr.afpa.controller.AuthenticateController;
import fr.afpa.model.User;
import fr.afpa.utilitary.ClassUtilitary;
import fr.afpa.utilitary.ConstantUtilitary;
import fr.afpa.view.IAppView;
import java.sql.SQLException;
import java.util.Scanner;

public class AppViewIMP implements IAppView{

    private static AppViewIMP instance;

    public static AppViewIMP getInstance() {
        if (instance == null) {
            instance = new AppViewIMP();
        }
        return instance;
    }

    public static final Scanner sc = new Scanner(System.in);
    private static final ClassUtilitary classUtilitary = ClassUtilitary.getInstance();
    private static final ConstantUtilitary constantUtilitary = ConstantUtilitary.getInstance();
    private static final AuthenticateController authentificateController = AuthenticateController.getInstance();

    @Override
    public User getUser(User user) {
        return new User(user.getMail());
    }

    @Override
    public void getPublicMenu() {
        System.out.println("    ******* Médiathèque *******");
        System.out.println();
        System.out.println("\t [1] - créer un compte ");
        System.out.println("\t [2] - se connecter ");
        System.out.println("\t [3] - afficher les oeuvres ");
        System.out.println("\t [Q] - quitter l'application ");
    }

    @Override
    public void getUserMenu(User user) {

        System.out.println("****** Bonjour " + user.getRole() + " " + user.getMail() + " *******");

        System.out.println();
        System.out.println("\t --- mon profil ---");
        System.out.println();

        //admin
        if (user.getRole().equalsIgnoreCase(constantUtilitary.ADMIN)){
            System.out.println("\t [A] - créer un utilisateur");
            System.out.println("\t [Z] - trouver un utilisateur");
        }

        //mediathecaire
        if (user.getRole().equalsIgnoreCase(constantUtilitary.MEDIATHECAIRE)){
            System.out.println("\t [Q] - créer un adhérent");
            System.out.println("\t [Z] - trouver un utilisateur");
            System.out.println("\t [F] - supprimer un utilisateur");
            System.out.println();
            System.out.println("\t [W] - enregistrer une oeuvre ");
            System.out.println("\t [X] - enregistrer un ouvrage ");
            System.out.println("\t [P] - valider un emprunt ");
            System.out.println("\t [O] - afficher les emprunts en cours ");
            System.out.println("\t [C] - supprimer une oeuvre ");
            System.out.println("\t [V] - supprimer un ouvrage ");
        }

        //adherent
        if (user.getRole().equalsIgnoreCase(constantUtilitary.ADHERENT)){
            System.out.println("\t [S] - afficher mon compte");
            System.out.println("\t [I] - modifier mon solde ");
            System.out.println("\t [D] - modifier mon compte ");
            System.out.println("\t [G] - supprimer mon compte ");
            System.out.println();
            System.out.println("\t [H] - reserver un ouvrage ");
            System.out.println("\t [J] - afficher les reservations en cours ");
            System.out.println("\t [K] - annuler une reservation ");
        }
        System.out.println();
        System.out.println("\t [0] - se deconnecter ");

    }


    @Override
    public User promptUser(User user) throws SQLException {

        long id;
        do{
            id = classUtilitary.generateId();
        }while(authentificateController.exist(id));

        String role;
        String mail;
        String password;
        double solde = 0.0;

        do {

            System.out.println("role : [admin, mediathecaire, adherent]");
            role = sc.nextLine();

        } while( !((role.equalsIgnoreCase(constantUtilitary.ADMIN))
                || (role.equalsIgnoreCase(constantUtilitary.MEDIATHECAIRE))
                || (role.equalsIgnoreCase(constantUtilitary.ADHERENT)) )
        );

        System.out.println("mail : ");
        mail = sc.nextLine();

        System.out.println("password : ");
        password = sc.nextLine();

        if(role.equalsIgnoreCase(constantUtilitary.ADHERENT)){
            System.out.println("solde : ");
            solde = Double.parseDouble(sc.nextLine());
        }

        return new User(id, mail, password, role, solde);
    }

    @Override
    public User promptMediathecaire(User user) throws SQLException {

        long id;
        do{
            id = classUtilitary.generateId();
        }while(authentificateController.exist(id));

        String role = constantUtilitary.ADHERENT;
        String mail;
        String password;
        double solde = 0.0;

        System.out.println("mail : ");
        mail = sc.nextLine();

        System.out.println("password : ");
        password = sc.nextLine();

        System.out.println("solde : ");
        solde = Double.parseDouble(sc.nextLine());

        return new User(id, mail, password, role, solde);
    }


    @Override
    public User promptFindUser(User user) {

        System.out.println("entrer un mail : ");
        String mail = sc.nextLine();

        return new User(mail);
    }

    @Override
    public User promptUpdateSolde(User user) {
        System.out.println("nouveau solde : ");
        user.setSolde(Double.parseDouble(sc.nextLine()));
        return user;
    }

    @Override
    public User promptUpdateUser(User user) {

        System.out.println("nouveau mail : ");
        user.setMail(sc.nextLine());

        System.out.println("nouveau password : ");
        user.setPassword(sc.nextLine());

        if(user.getRole().equalsIgnoreCase(constantUtilitary.ADHERENT)) {
            System.out.println("nouveau solde : ");
            user.setSolde(Double.parseDouble(sc.nextLine()));
        }

        return user;
    }

    @Override
    public Ouvrage promptUpdateReservations(Ouvrage ouvrage) {
        System.out.println("saisir l'id de l'ouvrage à renseigner pour la réservation : ");
        ouvrage.setId(Integer.parseInt(sc.nextLine()));
        return ouvrage;
    }


    @Override
    public User promptDeleteUser(User user) {

        System.out.println("saisir le mail à supprimer");
        String mail = sc.nextLine();

        return new User(mail);
    }
}
