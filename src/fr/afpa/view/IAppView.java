package fr.afpa.view;

import fr.afpa.model.Ouvrage;
import fr.afpa.model.User;

import java.sql.SQLException;

public interface IAppView {
    User getUser(User user);
    void getPublicMenu();
    void getUserMenu(User user);
    User promptUser(User user) throws SQLException;
    User promptMediathecaire(User user) throws SQLException;
    User promptFindUser(User user);
    User promptUpdateSolde(User user) throws SQLException;
    User promptUpdateUser(User user) throws SQLException;
    Ouvrage promptUpdateReservations(Ouvrage ouvrage) throws SQLException;
    User promptDeleteUser(User user);
}
