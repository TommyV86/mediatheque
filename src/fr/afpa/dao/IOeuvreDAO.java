package fr.afpa.dao;

import fr.afpa.model.User;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface IOeuvreDAO<T> {
    void create(T t) throws SQLException;

    T findById(long id) throws SQLException;

    void update(T t) throws SQLException;

    void delete(T t) throws SQLException;

    void findAll() throws SQLException;

    boolean exist(long id) throws SQLException;

}
