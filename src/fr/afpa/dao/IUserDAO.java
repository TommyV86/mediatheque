package fr.afpa.dao;

import java.sql.SQLException;

public interface IUserDAO<T> {
    void create(T t) throws SQLException;
    T find(T t) throws SQLException;
    void updateSolde(T user) throws SQLException;
    void update(T t) throws SQLException;
    void delete(T t) throws SQLException;
    boolean exist(long id) throws SQLException;
}
