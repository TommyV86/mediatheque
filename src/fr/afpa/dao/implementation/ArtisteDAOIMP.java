package fr.afpa.dao.implementation;

import fr.afpa.dao.IOeuvreDAO;
import fr.afpa.database.MediathequeDB;
import fr.afpa.model.Artiste;
import fr.afpa.model.User;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ArtisteDAOIMP implements IOeuvreDAO<Artiste> {

    private static MediathequeDB mediathequeDB;

    private static ArtisteDAOIMP instance;

    public static ArtisteDAOIMP getInstance() {
        if (instance == null) {
            instance = new ArtisteDAOIMP();
        }
        return instance;
    }

    public ArtisteDAOIMP() {
        mediathequeDB = MediathequeDB.getInstance();
    }

    @Override
    public void create(Artiste artiste) throws SQLException {
        PreparedStatement preparedStatement = mediathequeDB.connect().prepareStatement("insert into Artiste (id,nom,categorie) values (?, ?, ?)");
        preparedStatement.setLong(1, artiste.getId());
        preparedStatement.setString(2, artiste.getNom());
        preparedStatement.setString(3, artiste.getMetier());
        preparedStatement.executeUpdate();
    }

    @Override
    public Artiste findById(long id) throws SQLException {
        Statement statement = mediathequeDB.connect().createStatement();
        ResultSet resultSet = statement.executeQuery("select * from Artiste where id = " + id);
        Artiste artiste = new Artiste();
        while (resultSet.next()) {
            artiste.setId(resultSet.getLong(1));
            artiste.setNom(resultSet.getString(2));
            artiste.setMetier(resultSet.getString(3));
        }
        return artiste;
    }

    @Override
    public void update(Artiste artiste) throws SQLException {
        PreparedStatement preparedStatement = mediathequeDB.connect().prepareStatement("update Artiste set nom = ?," +
                "categorie = ? where id = " + artiste.getId());
        preparedStatement.setString(1, artiste.getNom());
        preparedStatement.setString(2, artiste.getMetier());
        preparedStatement.executeUpdate();
    }

    @Override
    public void delete(Artiste artiste) throws SQLException {
        PreparedStatement preparedStatement = mediathequeDB.connect().prepareStatement("delete from Artiste where id = " + artiste.getId());
        preparedStatement.executeUpdate();
    }

    //À tester ToDo

    @Override
    public void findAll() throws SQLException {
        Statement statement = mediathequeDB.connect().createStatement();
        ResultSet resultSet = statement.executeQuery("select * from Artiste");
        Artiste artiste = new Artiste();
        while (resultSet.next()) {
            artiste.setId(resultSet.getLong(1));
            artiste.setNom(resultSet.getString(2));
            artiste.setMetier(resultSet.getString(3));
            System.out.println(artiste.toString());
        }
    }

    @Override
    public boolean exist(long idArtiste) throws SQLException {
        boolean exist = false;
        Long id = null;
        ResultSet result = mediathequeDB.connect()
                .createStatement(
                ).executeQuery(
                        "SELECT id FROM artiste WHERE id = '" + idArtiste + "'"
                );
        while (result.next()) {
            id = result.getLong("id");
        }
        if (id != null)
            exist = true;
        return exist;
    }
}
