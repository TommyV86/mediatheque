package fr.afpa.dao.implementation;

import fr.afpa.dao.IUserDAO;
import fr.afpa.database.MediathequeDB;
import fr.afpa.model.User;
import fr.afpa.utilitary.ConstantUtilitary;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class AuthenticateDaoIMP implements IUserDAO<User> {
    private static AuthenticateDaoIMP instance;
    public static AuthenticateDaoIMP getInstance() {
        if (instance == null) {
            instance = new AuthenticateDaoIMP();
        }
        return instance;
    }

    private static final ConstantUtilitary constantUtilitary = ConstantUtilitary.getInstance();
    private static final MediathequeDB mediathequeDB = MediathequeDB.getInstance();


    @Override
    public void create(User user) throws SQLException {

        PreparedStatement prepare =
                mediathequeDB.connect().prepareStatement(
                        "INSERT INTO utilisateur (id, mail, password, role, solde) VALUES(?, ?, ?, ?, ?)"
                );

        prepare.setLong(1, user.getId());
        prepare.setString(2, user.getMail());
        prepare.setString(3, user.getPassword());
        prepare.setString(4, user.getRole());
        prepare.setDouble(5, user.getSolde());

        prepare.executeUpdate();
    }

    @Override
    public User find(User user) throws SQLException {

        ResultSet result = mediathequeDB.connect()
                .createStatement(
                ).executeQuery(
                        "SELECT id, mail, role, solde FROM utilisateur WHERE mail = '" + user.getMail() + "'"
                );
        while (result.next()){
            user.setId(result.getLong("id"));
            user.setMail(result.getString("mail"));
            user.setRole(result.getString("role"));
            user.setSolde(result.getDouble("solde"));
        }

        return user;
    }

    @Override
    public void update(User user) throws SQLException {

        mediathequeDB.connect()
        .createStatement()
        .executeUpdate(
                "UPDATE utilisateur SET mail = '" + user.getMail() + "'"
                  + ", password = '" + user.getPassword() + "'"
                  + ", solde = "
                  + (user.getRole().equalsIgnoreCase(constantUtilitary.ADHERENT) ? user.getSolde() : 0)
                  + " WHERE id = " + user.getId()+ " AND role != '" + constantUtilitary.ADMIN + "'"
        );
    }

    public void updateSolde(User user) throws SQLException {

        mediathequeDB.connect()
                .createStatement()
                .executeUpdate("UPDATE utilisateur SET solde = "+ user.getSolde() + " WHERE id = " + user.getId()) ;
    }

    @Override
    public void delete(User user) throws SQLException {

        mediathequeDB.connect()
        .createStatement(
        ).executeUpdate(
                "DELETE FROM utilisateur " +
                    "WHERE mail = '" + user.getMail() +
                    "' AND role != '"+ constantUtilitary.ADMIN +
                    "' AND role != '" + constantUtilitary.MEDIATHECAIRE + "'"
        );
    }


    @Override
    public boolean exist(long idUser) throws SQLException {
        boolean exist = false;
        Long id = null;
        ResultSet result = mediathequeDB.connect()
                .createStatement(
                ).executeQuery(
                        "SELECT id FROM utilisateur WHERE id = '" + idUser + "'"
                );
        while (result.next()){
            id = result.getLong("id");
        }
        if(id != null)
            exist = true;
        return exist;
    }
}
