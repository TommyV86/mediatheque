package fr.afpa.dao.implementation;

import fr.afpa.dao.IOeuvreDAO;
import fr.afpa.database.MediathequeDB;
import fr.afpa.model.Oeuvre;
import fr.afpa.model.Ouvrage;
import fr.afpa.model.User;
import fr.afpa.utilitary.ClassUtilitary;

import java.sql.*;

public class OuvrageDAOIMP implements IOeuvreDAO<Ouvrage> {
    private static MediathequeDB mediathequeDB;
    private static final ClassUtilitary classUtilitary = ClassUtilitary.getInstance();
    private static OuvrageDAOIMP instance;

    public static OuvrageDAOIMP getInstance() {
        if (instance == null) {
            instance = new OuvrageDAOIMP();
        }
        return instance;
    }

    public OuvrageDAOIMP() {
        mediathequeDB = MediathequeDB.getInstance();
    }

    @Override
    public void create(Ouvrage ouvrage) throws SQLException {
        ouvrage.setId(classUtilitary.generateId());
        PreparedStatement preparedStatement = mediathequeDB.connect().
                prepareStatement(
                        "insert into ouvrage (id, dispo, etat, prix, dateachat, id_oeuvre) "
                          + "values (?, ?, ?, ?, ?, ?)");
        preparedStatement.setLong(1, ouvrage.getId());
        preparedStatement.setBoolean(2, ouvrage.getDispo());
        preparedStatement.setBoolean(3, ouvrage.getEtat());
        preparedStatement.setDouble(4, ouvrage.getPrix());
        preparedStatement.setDate(5, ouvrage.getDateAchat());
        preparedStatement.setLong(6, ouvrage.getIdOeuvre());
        preparedStatement.executeUpdate();
        updateIdUser(ouvrage);
    }

    @Override
    public Ouvrage findById(long id) throws SQLException {
        Statement statement = mediathequeDB.connect().createStatement();
        ResultSet resultSet = statement.executeQuery("select * from ouvrage where id = " + id);
        Ouvrage ouvrage = new Ouvrage();
        while (resultSet.next()) {
            ouvrage.setId(resultSet.getLong(1));
            ouvrage.setDispo(resultSet.getBoolean(2));
            ouvrage.setEtat(resultSet.getBoolean(3));
            ouvrage.setPrix(resultSet.getDouble(4));
            ouvrage.setDateAchat(resultSet.getDate(5));
            ouvrage.setIdOeuvre(resultSet.getLong(6));
            ouvrage.setIdUtilisateur(resultSet.getInt(7));
        }
        return ouvrage;
    }

    @Override
    public void update(Ouvrage ouvrage) throws SQLException {
        PreparedStatement preparedStatement = mediathequeDB.connect().prepareStatement("update ouvrage set dispo = ?, etat = ?, " +
                " prix = ?, dateachat = ?, id_oeuvre = ? where id = " + ouvrage.getId());
        preparedStatement.setBoolean(1, ouvrage.getDispo());
        preparedStatement.setBoolean(2, ouvrage.getEtat());
        preparedStatement.setDouble(3, ouvrage.getPrix());
        preparedStatement.setDate(4, ouvrage.getDateAchat());
        preparedStatement.setLong(5, ouvrage.getIdOeuvre());
        updateIdUser(ouvrage);
        preparedStatement.executeUpdate();
    }

    private void updateIdUser(Ouvrage ouvrage) throws SQLException {
        PreparedStatement preparedStatement = mediathequeDB.connect().prepareStatement("update ouvrage set id_utilisateur = ? where id = " + ouvrage.getId());
        if (ouvrage.getIdUtilisateur() != null) {
            preparedStatement.setInt(1, ouvrage.getIdUtilisateur());
        } else {
            preparedStatement.setNull(1, Types.INTEGER);
        }
        preparedStatement.executeUpdate();
    }

    @Override
    public void delete(Ouvrage ouvrage) throws SQLException {
        PreparedStatement preparedStatement = mediathequeDB.connect().prepareStatement("delete from ouvrage where id = " + ouvrage.getId());
        preparedStatement.executeUpdate();
    }

    //À tester ToDo

    @Override
    public void findAll() throws SQLException {
        Statement statement = mediathequeDB.connect().createStatement();
        ResultSet resultSet = statement.executeQuery("select * from ouvrage");
        Ouvrage ouvrage = new Ouvrage();
        while (resultSet.next()) {
            ouvrage.setId(resultSet.getLong(1));
            ouvrage.setDispo(resultSet.getBoolean(2));
            ouvrage.setEtat(resultSet.getBoolean(3));
            ouvrage.setPrix(resultSet.getDouble(4));
            ouvrage.setDateAchat(resultSet.getDate(5));
            ouvrage.setIdOeuvre(resultSet.getLong(6));
            ouvrage.setIdUtilisateur(resultSet.getInt(7));
            System.out.println(ouvrage.toString());
        }
    }

    @Override
    public boolean exist(long idOuvrage) throws SQLException {
        boolean exist = false;
        Long id = null;
        ResultSet result = mediathequeDB.connect()
                .createStatement(
                ).executeQuery(
                        "SELECT id FROM ouvrage WHERE id = '" + idOuvrage + "'"
                );
        while (result.next()) {
            id = result.getLong("id");
        }
        if (id != null)
            exist = true;
        return exist;
    }

    public void findAll(Oeuvre oeuvre) throws SQLException {
        Statement statement = mediathequeDB.connect().createStatement();
        ResultSet resultSet = statement.executeQuery("select * from ouvrage where id_oeuvre = " + oeuvre.getId());
        Ouvrage ouvrage = new Ouvrage();
        while (resultSet.next()) {
            ouvrage.setId(resultSet.getLong(1));
            ouvrage.setDispo(resultSet.getBoolean(2));
            ouvrage.setEtat(resultSet.getBoolean(3));
            ouvrage.setPrix(resultSet.getDouble(4));
            ouvrage.setDateAchat(resultSet.getDate(5));
            ouvrage.setIdOeuvre(resultSet.getLong(6));
            ouvrage.setIdUtilisateur(resultSet.getInt(7));
            System.out.println(ouvrage.toString());
        }
    }

    public void findReservations() throws SQLException {
        long id_user;
        long id_ouvrage;
        boolean isReserve;


        ResultSet results = mediathequeDB.connect()
                .createStatement().executeQuery(
                        "SELECT * FROM louer"
                );
        while (results.next()) {
            id_user = results.getLong("id_utilisateur");
            id_ouvrage = results.getLong("id_ouvrage");
            isReserve = results.getBoolean("reserve");

            System.lineSeparator();
            System.out.println("id_user : " + id_user
                    + ", id_ouvrage : " + id_ouvrage
                    + ", reservation : " + isReserve);
            System.lineSeparator();
        }
    }

    public void updateReservations(Ouvrage ouvrage) throws SQLException{
        mediathequeDB.connect().createStatement().executeUpdate(
                "UPDATE louer SET reserve = true WHERE id_ouvrage = "+ ouvrage.getId() +""
        );
        mediathequeDB.connect().createStatement().executeUpdate(
                "UPDATE ouvrage SET id_utilisateur = "
                        + "(SELECT id_utilisateur FROM louer WHERE id_ouvrage = "+ ouvrage.getId()+") "
                        + "WHERE id = "+ouvrage.getId()
        );
    }

    public Ouvrage findByIdOeuvre(long id) throws SQLException {
        Statement statement = mediathequeDB.connect().createStatement();
        ResultSet resultSet = statement.executeQuery("select * from ouvrage where id_oeuvre = " + id);
        Ouvrage ouvrage = new Ouvrage();
        while (resultSet.next()) {
            ouvrage.setId(resultSet.getLong(1));
            ouvrage.setDispo(resultSet.getBoolean(2));
            ouvrage.setEtat(resultSet.getBoolean(3));
            ouvrage.setPrix(resultSet.getDouble(4));
            ouvrage.setDateAchat(resultSet.getDate(5));
            ouvrage.setIdOeuvre(resultSet.getLong(6));
            ouvrage.setIdUtilisateur(resultSet.getInt(7));
        }
        return ouvrage;
    }

    public Ouvrage findByIdUtilisateur(long id) throws SQLException {
        Statement statement = mediathequeDB.connect().createStatement();
        ResultSet resultSet = statement.executeQuery("select * from ouvrage where id_utilisateur = " + id);
        Ouvrage ouvrage = new Ouvrage();
        while (resultSet.next()) {
            ouvrage.setId(resultSet.getLong(1));
            ouvrage.setDispo(resultSet.getBoolean(2));
            ouvrage.setEtat(resultSet.getBoolean(3));
            ouvrage.setPrix(resultSet.getDouble(4));
            ouvrage.setDateAchat(resultSet.getDate(5));
            ouvrage.setIdOeuvre(resultSet.getLong(6));
            ouvrage.setIdUtilisateur(resultSet.getInt(7));
        }
        return ouvrage;
    }

    public void reserver(User user, Ouvrage ouvrage) throws SQLException {

        PreparedStatement preparedStatement =
                mediathequeDB.connect().prepareStatement(
                        "insert into louer (id_utilisateur, id_ouvrage, reserve) values (?, ?, ?)");
        preparedStatement.setLong(1, user.getId());
        preparedStatement.setLong(2, ouvrage.getId());
        preparedStatement.setBoolean(3, false);

        preparedStatement.executeUpdate();
    }

    public void afficherReservationsUtilisateur(User user) throws SQLException {
        Statement statement = mediathequeDB.connect().createStatement();
        ResultSet resultSet = statement.executeQuery("select * from louer where id_utilisateur = " + user.getId());
        long idUtilisateur;
        long idOuvrage;
        boolean boolReserve;

        while (resultSet.next()) {
            idOuvrage = resultSet.getLong(1);
            idUtilisateur = resultSet.getLong(2);
            boolReserve = resultSet.getBoolean(3);
            String reserve;
            if (boolReserve)
                reserve = "acceptée";
            else
                reserve = "en attente de validation";
            System.out.println("id utilisateur : " + idUtilisateur + System.lineSeparator()
                    + "Id ouvrage : " + idOuvrage + ", réservation acceptée : " + reserve);
        }
    }

    public void annulerReservation(User user, Ouvrage ouvrage) throws SQLException {
        Statement statement = mediathequeDB.connect().createStatement();
        ResultSet resultSet = statement.executeQuery("select reserve from louer where id_utilisateur = " + user.getId() + " and id_ouvrage = " + ouvrage.getId());
        boolean reserve = false;
        while (resultSet.next())
            reserve = !resultSet.getBoolean(1);
        PreparedStatement preparedStatement;
        if (reserve) {
            preparedStatement = mediathequeDB.connect().prepareStatement("delete from louer where id_utilisateur = " + user.getId() + " and id_ouvrage = " + ouvrage.getId());
            preparedStatement.executeUpdate();
        } else
            System.out.println("Vous ne pouvez pas annuler une réservation déjà en cours");
    }
}
