package fr.afpa.dao.implementation;

import fr.afpa.dao.IOeuvreDAO;
import fr.afpa.database.MediathequeDB;
import fr.afpa.model.Artiste;
import fr.afpa.model.Oeuvre;
import fr.afpa.utilitary.ClassUtilitary;

import java.sql.*;

public class OeuvreDAOIMP implements IOeuvreDAO<Oeuvre> {
    private static MediathequeDB mediathequeDB;
    private static ClassUtilitary classUtilitary = ClassUtilitary.getInstance();
    private static OeuvreDAOIMP instance;

    public static OeuvreDAOIMP getInstance() {
        if (instance == null) {
            instance = new OeuvreDAOIMP();
        }
        return instance;
    }

    public OeuvreDAOIMP() {
        mediathequeDB = MediathequeDB.getInstance();
    }

    @Override
    public void create(Oeuvre oeuvre) throws SQLException {

        oeuvre.setId(classUtilitary.generateId());

        PreparedStatement preparedStatement = mediathequeDB.connect().
                prepareStatement("insert into Oeuvre (id, titre, nbredispo, genre, anneeparution) values (?, ?, ?, ?, ?)");
        preparedStatement.setLong(1, oeuvre.getId());
        preparedStatement.setString(2, oeuvre.getTitre());
        preparedStatement.setLong(3, oeuvre.getNbreDispo());
        preparedStatement.setString(4, oeuvre.getGenre());
        preparedStatement.setDate(5, oeuvre.getAnneeParution());
        preparedStatement.executeUpdate();
    }

    @Override
    public Oeuvre findById(long id) throws SQLException {
        Statement statement = mediathequeDB.connect().createStatement();
        ResultSet resultSet = statement.executeQuery("select * from Oeuvre where id = " + id);
        Oeuvre oeuvre = new Oeuvre();
        while (resultSet.next()) {
            oeuvre.setId(resultSet.getLong(1));
            oeuvre.setTitre(resultSet.getString(2));
            oeuvre.setNbreDispo(resultSet.getInt(3));
            oeuvre.setGenre(resultSet.getString(4));
            oeuvre.setAnneeParution(resultSet.getDate(5));
        }
        return oeuvre;
    }

    @Override
    public void update(Oeuvre oeuvre) throws SQLException {
        PreparedStatement preparedStatement = mediathequeDB.connect().prepareStatement("update Oeuvre set titre = ?, nbredispo = ?, " +
                " genre = ?, anneeparution = ? where id = " + oeuvre.getId());
        preparedStatement.setString(1, oeuvre.getTitre());
        preparedStatement.setLong(2, oeuvre.getNbreDispo());
        preparedStatement.setString(3, oeuvre.getGenre());
        preparedStatement.setDate(4, oeuvre.getAnneeParution());
        preparedStatement.executeUpdate();
    }

    @Override
    public void delete(Oeuvre oeuvre) throws SQLException {
        PreparedStatement preparedStatement = mediathequeDB.connect().prepareStatement("delete from Oeuvre where id = " + oeuvre.getId());
        preparedStatement.executeUpdate();
    }

    //À tester ToDo

    @Override
    public void findAll() throws SQLException {
        Statement statement = mediathequeDB.connect().createStatement();
        ResultSet resultSet = statement.executeQuery("select * from Oeuvre");
        Oeuvre oeuvre = new Oeuvre();
        while (resultSet.next()) {
            oeuvre.setId(resultSet.getLong(1));
            oeuvre.setTitre(resultSet.getString(2));
            oeuvre.setNbreDispo(resultSet.getInt(3));
            oeuvre.setGenre(resultSet.getString(4));
            oeuvre.setAnneeParution(resultSet.getDate(5));
            System.out.println(oeuvre.toString());
        }
    }

    @Override
    public boolean exist(long idOeuvre) throws SQLException {
        boolean exist = false;
        Long id = null;
        ResultSet result = mediathequeDB.connect()
                .createStatement(
                ).executeQuery(
                        "SELECT id FROM oeuvre WHERE id = '" + idOeuvre + "'"
                );
        while (result.next()) {
            id = result.getLong("id");
        }
        if (id != null)
            exist = true;
        return exist;
    }

    public Oeuvre findByTitle(String title) throws SQLException {
        Statement statement = mediathequeDB.connect().createStatement();
        ResultSet resultSet = statement.executeQuery("select * from Oeuvre where titre = '" + title + "'");
        Oeuvre oeuvre = new Oeuvre();
        while (resultSet.next()) {
            oeuvre.setId(resultSet.getLong(1));
            oeuvre.setTitre(resultSet.getString(2));
            oeuvre.setNbreDispo(resultSet.getInt(3));
            oeuvre.setGenre(resultSet.getString(4));
            oeuvre.setAnneeParution(resultSet.getDate(5));
        }
        return oeuvre;
    }
}
