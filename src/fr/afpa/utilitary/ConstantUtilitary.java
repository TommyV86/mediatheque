package fr.afpa.utilitary;

import fr.afpa.service.implementation.OuvrageServiceIMP;

import java.util.Scanner;

public class ConstantUtilitary {

    private static ConstantUtilitary instance;

    public static ConstantUtilitary getInstance() {
        if (instance == null) {
            instance = new ConstantUtilitary();
        }
        return instance;
    }


    public final String URL = "jdbc:postgresql://localhost/mediatheque";
    public final String USER_NAME = "postgres";
    public final String PASSWORD = "12564379"; //à adapter selon le poste de travail (mdp du compte SQL)
    public final String ADMIN = "admin";
    public final String MEDIATHECAIRE  = "mediathecaire";
    public final String ADHERENT = "adherent";
}
