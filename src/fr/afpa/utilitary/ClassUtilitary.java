package fr.afpa.utilitary;

import java.util.Random;

public class ClassUtilitary {


    public static ClassUtilitary instance;
    public static ClassUtilitary getInstance() {
        if (instance == null) {
            instance = new ClassUtilitary();
        }
        return instance;
    }

    public Long generateId() {
        Random ran = new Random();
        return (long) ran.nextInt(1000000);
    }
}
