package fr.afpa.model;

public class User {

    private long id;
    private String mail;
    private String password;
    private String role;
    private Double solde;


    public User(){}

    public User(long id) {
        this.id = id;
    }
    public User(String mail){
        this.mail = mail;
    }
    public User(String mail, String password){
        this.mail = mail;
        this.password= password;
    }

    public User(long id, String mail, String password, String role, Double solde) {
        this.id = id;
        this.mail = mail;
        this.password = password;
        this.role = role;
        this.solde = solde;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Double getSolde() {
        return solde;
    }

    public void setSolde(Double solde) {
        this.solde = solde;
    }

    @Override
    public String toString() {
        return  System.lineSeparator() + "id = " + id +
                System.lineSeparator() + "mail = " + mail +
                System.lineSeparator() + "role = " + role +
                System.lineSeparator() + (
                        role.equalsIgnoreCase("adherent") ? "solde = " + solde + " € " : "  ") +
                System.lineSeparator() + "";
    }
}
