package fr.afpa.model;

import java.sql.Date;

public class Oeuvre {
    private long id;
    private String titre;
    private int nbreDispo;
    private String genre;
    private Date anneeParution;

    public Oeuvre() {

    }

    public Oeuvre(int id, String titre, int nbreDispo, String genre, Date anneeParution) {
        this.id = id;
        this.titre = titre;
        this.nbreDispo = nbreDispo;
        this.genre = genre;
        this.anneeParution = anneeParution;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public int getNbreDispo() {
        return nbreDispo;
    }

    public void setNbreDispo(int nbreDispo) {
        this.nbreDispo = nbreDispo;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public Date getAnneeParution() {
        return anneeParution;
    }

    public void setAnneeParution(Date anneeParution) {
        this.anneeParution = anneeParution;
    }

    @Override
    public String toString() {
        return "Id : " + id + ", Genre : " + genre + ", Titre : " + titre;
    }
}
