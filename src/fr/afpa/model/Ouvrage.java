package fr.afpa.model;

import java.sql.Date;

public class Ouvrage {
    private long id;
    private long idOeuvre;
    private Integer idUtilisateur = null;
    private boolean dispo;
    private boolean etat;
    private double prix;
    private Date dateAchat;

    public Ouvrage() {

    }

    public Ouvrage(long id, long idOeuvre,Integer idUtilisateur, boolean dispo, boolean etat, double prix, Date dateAchat) {
        this.id = id;
        this.idOeuvre = idOeuvre;
        this.idUtilisateur = idUtilisateur;
        this.dispo = dispo;
        this.etat = etat;
        this.prix = prix;
        this.dateAchat = dateAchat;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getIdOeuvre() {
        return idOeuvre;
    }

    public void setIdOeuvre(long idOeuvre) {
        this.idOeuvre = idOeuvre;
    }

    public Integer getIdUtilisateur() {
        return idUtilisateur;
    }

    public void setIdUtilisateur(Integer idUtilisateur) {
        this.idUtilisateur = idUtilisateur;
    }

    public boolean getDispo() {
        return dispo;
    }

    public void setDispo(boolean dispo) {
        this.dispo = dispo;
    }

    public boolean getEtat() {
        return etat;
    }

    public void setEtat(boolean etat) {
        this.etat = etat;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    public Date getDateAchat() {
        return dateAchat;
    }

    public void setDateAchat(Date dateAchat) {
        this.dateAchat = dateAchat;
    }

    @Override
    public String toString() {
        //ToDo
        return "Id de l'oeuvre : " + idOeuvre + System.lineSeparator()
                + "Disponible : " + this.dispoToString() + System.lineSeparator()
                + "État : " + this.etatToString() + ", prix de l'ouvrage : " + prix;
    }

    private String dispoToString() {
        if (dispo)
            return "disponible";
        else
            return "indisponible";
    }

    private String etatToString() {
        if (etat)
            return "bon état";
        else
            return "abimé";
    }
}
