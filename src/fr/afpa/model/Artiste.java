package fr.afpa.model;

public class Artiste {
    private long id;
    private String nom;
    private String metier;

    public Artiste() {

    }

    public Artiste(long id, String nom, String metier) {
        this.id = id;
        this.nom = nom;
        this.metier = metier;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getMetier() {
        return metier;
    }

    public void setMetier(String metier) {
        this.metier = metier;
    }

    public String toString() {
        return "Nom : " + nom + ", métier : " + metier;
    }
}
