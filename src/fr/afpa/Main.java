package fr.afpa;

import fr.afpa.controller.AppViewController;
import fr.afpa.controller.AuthenticateController;
import fr.afpa.controller.implementation.OeuvreControllerIMP;
import fr.afpa.controller.implementation.OuvrageControllerIMP;
import fr.afpa.controller.ILocationsOuvrage;
import fr.afpa.model.Ouvrage;
import fr.afpa.model.User;
import fr.afpa.utilitary.ConstantUtilitary;

import java.sql.SQLException;
import java.util.Scanner;

public class Main {

    private static final Scanner sc = new Scanner(System.in);
    private static final AppViewController appViewController = AppViewController.getInstance();
    private static final AuthenticateController authenticateController = AuthenticateController.getInstance();
    private static final OeuvreControllerIMP oeuvreController = OeuvreControllerIMP.getInstance();
    private static final OuvrageControllerIMP ouvrageController = OuvrageControllerIMP.getInstance();
    private static final ConstantUtilitary constantUtilitary = ConstantUtilitary.getInstance();
    private static final ILocationsOuvrage<Ouvrage> locationOuvrageController = OuvrageControllerIMP.getInstance();


    private static User user = new User();
    private static Ouvrage ouvrage = new Ouvrage();


    private static String choice;



    public static void main(String[] args) throws SQLException {

        System.out.println("entrer un code admin ou taper entrée : ");
        String code = sc.nextLine();

        if (code.equalsIgnoreCase(constantUtilitary.ADMIN)){
            authenticateController.create(user);
        }

        do{
            appViewController.getPublicMenu();
            choice = sc.nextLine();

            switch (choice) {
                case "1":
                    authenticateController.createAdherent(user);
                break;
                case "2":
                    //connexion
                    user = authenticateController.find(user);

                    System.out.println(user.toString());

                    if (user.getRole().equalsIgnoreCase(constantUtilitary.ADMIN) ||
                        user.getRole().equalsIgnoreCase(constantUtilitary.MEDIATHECAIRE) ||
                        user.getRole().equalsIgnoreCase(constantUtilitary.ADHERENT)) {

                        do {

                            appViewController.getUserMenu(user);
                            choice = sc.nextLine();

                            switch (choice) {
                                //admin et mediathecaire
                                case "a":
                                    if (user.getRole().equalsIgnoreCase(constantUtilitary.ADMIN))
                                        authenticateController.create(user);
                                    break;
                                case "z":
                                    if (user.getRole().equalsIgnoreCase(constantUtilitary.ADMIN) ||
                                        user.getRole().equalsIgnoreCase(constantUtilitary.MEDIATHECAIRE)){
                                            System.out.println(authenticateController.find(user).toString());
                                            sc.nextLine();
                                        }
                                    break;
                                //mediathecaire et adherent
                                case "s":
                                    if (user.getRole().equalsIgnoreCase(constantUtilitary.ADHERENT)) {
                                        System.out.println(authenticateController.getUser(user).toString());
                                        sc.nextLine();
                                    }
                                    break;
                                case "q":
                                    if (user.getRole().equalsIgnoreCase(constantUtilitary.MEDIATHECAIRE)) {
                                        authenticateController.createAdherent(user);
                                    }
                                    break;
                                case "w":
                                    if (user.getRole().equalsIgnoreCase(constantUtilitary.MEDIATHECAIRE)){
                                        oeuvreController.enregistrer();
                                        sc.nextLine();
                                    }
                                    break;
                                case "x":
                                    if (user.getRole().equalsIgnoreCase(constantUtilitary.MEDIATHECAIRE)){
                                        ouvrageController.enregistrer();
                                        sc.nextLine();
                                    }
                                    break;
                                case "p":
                                    if (user.getRole().equalsIgnoreCase(constantUtilitary.MEDIATHECAIRE)){
                                        locationOuvrageController.updateReservations(ouvrage);
                                    }
                                    break;
                                case "o":
                                    if (user.getRole().equalsIgnoreCase(constantUtilitary.MEDIATHECAIRE)){
                                        locationOuvrageController.findReservations();
                                        sc.nextLine();
                                    }
                                    break;
                                case "c":
                                    if (user.getRole().equalsIgnoreCase(constantUtilitary.MEDIATHECAIRE)){
                                        oeuvreController.supprimer();
                                        sc.nextLine();
                                    }
                                    break;
                                case "v":
                                    if (user.getRole().equalsIgnoreCase(constantUtilitary.MEDIATHECAIRE)){
                                        ouvrageController.supprimer();
                                        sc.nextLine();
                                    }
                                    break;
                                case "i":
                                    if (user.getRole().equalsIgnoreCase(constantUtilitary.ADHERENT)) {
                                        authenticateController.updateSolde(user);
                                    }
                                    break;
                                case "d":
                                    if (user.getRole().equalsIgnoreCase(constantUtilitary.ADHERENT)) {
                                        authenticateController.update(user);
                                    }
                                    break;
                                case "f":
                                    if (user.getRole().equalsIgnoreCase(constantUtilitary.MEDIATHECAIRE)) {
                                        authenticateController.delete(user);
                                    }
                                    break;
                                case "h":
                                    if(user.getRole().equalsIgnoreCase(constantUtilitary.ADHERENT)){
                                        ouvrageController.reserver(user);
                                    }
                                    break;
                                case "j":
                                    if(user.getRole().equalsIgnoreCase(constantUtilitary.ADHERENT)){
                                        ouvrageController.affichageReservationsUtilisateur(user);
                                    }
                                    break;
                                case "k":
                                    if(user.getRole().equalsIgnoreCase(constantUtilitary.ADHERENT)){
                                        ouvrageController.annulerReservation(user);
                                    }
                                    break;
                                case "g":
                                    if (user.getRole().equalsIgnoreCase(constantUtilitary.ADHERENT)) {
                                        authenticateController.getDelete(user);
                                        System.out.println("compte supprimé");
                                        sc.nextLine();
                                        choice = "0";
                                    }
                                    break;
                                default:
                                    if(!choice.equals("0")){
                                        System.out.println("Erreur, saisie invalide !");
                                    }
                                    break;
                            }
                        } while (!choice.equals("0"));
                    }
                    break;
                case "3":
                    oeuvreController.showAll();
                    sc.nextLine();
                    break;
                default:
                    break;
            }
        } while (!choice.equalsIgnoreCase("Q"));
    }
}