package fr.afpa.service;

import fr.afpa.model.Oeuvre;
import fr.afpa.model.User;

import java.sql.SQLException;
import java.text.ParseException;

public interface IService<T> {
    T findById() throws SQLException;
    void enregistrer() throws ParseException, SQLException;
    void supprimer() throws SQLException;
    void showAll() throws SQLException;
    boolean exist(long id) throws SQLException;
}
