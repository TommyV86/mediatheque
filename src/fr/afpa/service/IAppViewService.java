package fr.afpa.service;

import fr.afpa.model.User;

public interface IAppViewService {
    void getMenu();
    void getUserMenu(User user);

}
