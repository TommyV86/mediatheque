package fr.afpa.service;

import fr.afpa.model.User;

import java.sql.SQLException;

public interface IAuthenticateService {
    void create(User user) throws SQLException;
    void createAdherent(User user) throws  SQLException;
    User find(User user) throws SQLException;
    User getUser(User user) throws SQLException;
    void updateSolde(User user)throws SQLException;
    void update(User user) throws SQLException;
    void delete(User promptDeleteUser) throws SQLException;
    boolean exist(long idUser) throws SQLException;
}
