package fr.afpa.service.implementation;

import fr.afpa.dao.IUserDAO;
import fr.afpa.dao.implementation.AuthenticateDaoIMP;
import fr.afpa.model.User;
import fr.afpa.service.IAuthenticateService;
import java.sql.SQLException;

public class AuthenticateServiceIMP implements IAuthenticateService {
    private static AuthenticateServiceIMP instance;
    public static AuthenticateServiceIMP getInstance() {
        if (instance == null) {
            instance = new AuthenticateServiceIMP();
        }
        return instance;
    }
    private static final IUserDAO<User> authenticateDAO = AuthenticateDaoIMP.getInstance();


    @Override
    public void create(User user) throws SQLException {
        authenticateDAO.create(user);
    }

    @Override
    public void createAdherent(User user) throws SQLException {
        authenticateDAO.create(user);
    }

    @Override
    public User find(User user) throws SQLException {
        return authenticateDAO.find(user);
    }

    @Override
    public User getUser(User user) throws SQLException {
        return authenticateDAO.find(user);
    }

    @Override
    public void updateSolde(User user) throws SQLException {
        authenticateDAO.updateSolde(user);
    }

    @Override
    public void update(User user) throws SQLException {
        authenticateDAO.update(user);
    }

    @Override
    public void delete(User user) throws SQLException {
        authenticateDAO.delete(user);
    }

    @Override
    public boolean exist(long idUser) throws SQLException {
        return authenticateDAO.exist(idUser);
    }

}
