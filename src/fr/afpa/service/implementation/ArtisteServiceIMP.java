package fr.afpa.service.implementation;

import fr.afpa.dao.implementation.ArtisteDAOIMP;
import fr.afpa.dao.implementation.OuvrageDAOIMP;
import fr.afpa.model.Artiste;
import fr.afpa.model.User;
import fr.afpa.service.IService;
import fr.afpa.utilitary.ClassUtilitary;
import fr.afpa.utilitary.ConstantUtilitary;

import java.sql.SQLException;

import static fr.afpa.view.implementation.AppViewIMP.sc;

public class ArtisteServiceIMP implements IService<Artiste> {

    private static final ArtisteDAOIMP artisteDAO = ArtisteDAOIMP.getInstance();
    private static ArtisteServiceIMP instance;
    private static final ClassUtilitary classUtilitary = ClassUtilitary.getInstance();


    public static ArtisteServiceIMP getInstance() {
        if (instance == null) {
            instance = new ArtisteServiceIMP();
        }
        return instance;
    }

    @Override
    public Artiste findById() throws SQLException {
        System.out.println("Recherche d'un(e) Artiste par Id :");
        System.lineSeparator();
        System.out.println("Saisir l'id de l'artiste que vous souhaitez consulter :");
        System.lineSeparator();
        String entry = sc.nextLine().trim();
        Artiste artiste = artisteDAO.findById(Long.parseLong(entry));
        return artiste;
    }

    @Override
    public void enregistrer() throws SQLException {
        System.lineSeparator();
        System.out.println("Enregistrement de l'artiste :");
        System.lineSeparator();
        Artiste artiste = new Artiste();
        do{
            artiste.setId(classUtilitary.generateId());
        }while (artisteDAO.exist(artiste.getId()));
        System.out.println("Saisir le nom de l'artiste :");
        System.lineSeparator();
        String entry = sc.nextLine().trim();
        artiste.setNom(entry);
        System.lineSeparator();
        System.out.println("Saisir la catégorie de l'artiste (Auteur/Compositeur/Acteur/Réalisateur) :");
        System.lineSeparator();
        entry = sc.nextLine().trim();
        artiste.setMetier(entry);
        artisteDAO.create(artiste);
        System.lineSeparator();
        System.out.println("Enregistrement terminé, l'artiste a bien été enregistré(e) !");
        System.lineSeparator();
    }

    @Override
    public void supprimer() throws SQLException {
        System.lineSeparator();
        System.out.println("Menu supprimer :");
        System.lineSeparator();
        System.out.println("Saisir l'id de l'artiste :");
        System.lineSeparator();
        String entry = sc.nextLine().trim();
        System.lineSeparator();
        Artiste artiste = artisteDAO.findById(Long.parseLong(entry));
        if (artiste == null) {
            System.out.println("Artiste introuvable");
            return;
        }
        System.out.println("Êtes vous sûr de vouloir supprimer l'artiste " + artiste.toString() + " ? (pressez la touche o pour confirmer)");
        System.lineSeparator();
        entry = sc.nextLine().trim();
        System.lineSeparator();
        if (!"o".equalsIgnoreCase(entry)) {
            System.out.println("Annulation de la suppression.");
            return;
        }
        System.out.println("Suppression en cours...");
        System.lineSeparator();
        artisteDAO.delete(artiste);
        System.out.println("L'artiste " + artiste.toString() + " a bien été supprimé(e).");
        System.lineSeparator();
    }

    @Override
    public void showAll() throws SQLException {
        System.lineSeparator();
        System.out.println("Affichage de tou(te)s les artistes :");
        System.lineSeparator();
        System.out.println("    *****   *****   *****");
        System.lineSeparator();
        artisteDAO.findAll();
        System.lineSeparator();
        System.out.println("    *****   *****   *****");
        System.lineSeparator();
    }

    @Override
    public boolean exist(long id) throws SQLException {
        return artisteDAO.exist(id);
    }
}
