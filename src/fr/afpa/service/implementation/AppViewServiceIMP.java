package fr.afpa.service.implementation;

import fr.afpa.model.User;
import fr.afpa.service.IAppViewService;
import fr.afpa.view.IAppView;
import fr.afpa.view.implementation.AppViewIMP;

public class AppViewServiceIMP implements IAppViewService {
    private final IAppView appView =AppViewIMP.getInstance();
    @Override
    public void getMenu() {
        appView.getPublicMenu();
    }
    @Override
    public void getUserMenu(User user) {
        appView.getUserMenu(user);
    }
}
