package fr.afpa.service.implementation;

import fr.afpa.controller.AuthenticateController;
import fr.afpa.dao.implementation.OeuvreDAOIMP;
import fr.afpa.dao.implementation.OuvrageDAOIMP;
import fr.afpa.model.Oeuvre;
import fr.afpa.model.Ouvrage;
import fr.afpa.model.User;
import fr.afpa.service.IService;
import fr.afpa.utilitary.ClassUtilitary;
import fr.afpa.utilitary.ConstantUtilitary;

import java.sql.Date;
import java.sql.SQLException;
import java.text.ParseException;

import static fr.afpa.view.implementation.AppViewIMP.sc;

public class OuvrageServiceIMP implements IService<Ouvrage> {
    private static final AuthenticateController authenticateController = AuthenticateController.getInstance();
    private static final OuvrageDAOIMP ouvrageDAO = OuvrageDAOIMP.getInstance();
    private static final OeuvreDAOIMP oeuvreDAO = OeuvreDAOIMP.getInstance();
    private static final OeuvreServiceIMP oeuvreService = OeuvreServiceIMP.getInstance();
    private static final ClassUtilitary classUtilitary = ClassUtilitary.getInstance();

    private static OuvrageServiceIMP instance;

    public static OuvrageServiceIMP getInstance() {
        if (instance == null) {
            instance = new OuvrageServiceIMP();
        }
        return instance;
    }

    @Override
    public Ouvrage findById() throws SQLException {
        System.lineSeparator();
        System.out.println("Recherche d'un ouvrage par Id :");
        System.lineSeparator();
        System.out.println("Saisir l'id de l'ouvrage que vous souhaitez consulter :");
        System.lineSeparator();
        String entry = sc.nextLine().trim();
        System.lineSeparator();
        Ouvrage ouvrage = ouvrageDAO.findById(Long.parseLong(entry));
        return ouvrage;
    }

    @Override
    public void enregistrer() throws ParseException, SQLException {
        System.lineSeparator();
                oeuvreService.showAll();
        System.lineSeparator();
        System.out.println("Enregistrement de l'ouvrage :");
        System.lineSeparator();
        Ouvrage ouvrage = new Ouvrage();
        do{
            ouvrage.setId(classUtilitary.generateId());
        }while(ouvrageDAO.exist(ouvrage.getId()));
        System.out.println("Saisir l'Id de l'oeuvre :");
        System.lineSeparator();
        String entry = sc.nextLine().trim();
        ouvrage.setIdOeuvre(Long.parseLong(entry));
        System.lineSeparator();
        System.out.println("Saisir l'Id de l'utilisateur (appuyez directement sur la touche \"entrée\" si il n'y en a pas) :");
        System.lineSeparator();
        entry = sc.nextLine().trim();
        if (entry == null || entry.isBlank())
            ouvrage.setIdUtilisateur(null);
        else
            ouvrage.setIdUtilisateur(Integer.parseInt(entry));
        System.lineSeparator();
        System.out.println("Saisir si l'ouvrage est disponible ([o]ui si l'oeuvre est dispo) :");
        System.lineSeparator();
        entry = sc.nextLine().trim();
        if ("o".equalsIgnoreCase(entry))
            ouvrage.setDispo(true);
        else
            ouvrage.setDispo(false);
        System.lineSeparator();
        System.out.println("Saisir l'état de l'ouvrage ([b]on si il est en bon état) :");
        System.lineSeparator();
        entry = sc.nextLine().trim();
        if ("b".equalsIgnoreCase(entry))
            ouvrage.setEtat(true);
        else
            ouvrage.setEtat(false);
        System.lineSeparator();
        System.out.println("Saisir le prix de l'ouvrage :");
        System.lineSeparator();
        entry = sc.nextLine().trim();
        ouvrage.setPrix(Double.parseDouble(entry));
        System.lineSeparator();
        System.out.println("Saisir la date de l'achat/location de l'ouvrage (yyyy-MM-dd):");
        System.lineSeparator();
        entry = sc.nextLine().trim();
        System.lineSeparator();
        Date date = Date.valueOf(entry);
        ouvrage.setDateAchat(date);
        ouvrageDAO.create(ouvrage);
        System.lineSeparator();
        System.out.println("Enregistrement terminé, l'ouvrage a bien été enregistré !");
        System.lineSeparator();
    }

    @Override
    public void supprimer() throws SQLException {
        System.lineSeparator();
        System.out.println("Menu supprimer :");
        System.lineSeparator();
        System.out.println("Saisir l'Id de l'ouvrage :");
        System.lineSeparator();
        String entry = sc.nextLine().trim();
        System.lineSeparator();
        Ouvrage ouvrage = ouvrageDAO.findById(Long.parseLong(entry));
        if (ouvrage == null) {
            System.out.println("Ouvrage introuvable");
            return;
        }
        System.out.println("Êtes vous sûr de vouloir supprimer l'ouvrage " + ouvrage.toString() + " ? (pressez la touche o pour confirmer)");
        System.lineSeparator();
        entry = sc.nextLine().trim();
        System.lineSeparator();
        if (!entry.equalsIgnoreCase("o")) {
            System.out.println("Annulation de la suppression.");
            return;
        }
        System.out.println("Suppression en cours...");
        System.lineSeparator();
        ouvrageDAO.delete(ouvrage);
        System.out.println("L'ouvrage " + ouvrage.toString() + " a bien été supprimée.");
        System.lineSeparator();
    }

    @Override
    public void showAll() throws SQLException {
        System.lineSeparator();
        System.out.println("Affichage de tous les ouvrages :");
        System.lineSeparator();
        System.out.println("    *****   *****   *****");
        System.lineSeparator();
        ouvrageDAO.findAll();
        System.lineSeparator();
        System.out.println("    *****   *****   *****");
        System.lineSeparator();
    }

    @Override
    public boolean exist(long id) throws SQLException {
        return ouvrageDAO.exist(id);
    }

    public void findReservations() throws SQLException {
        ouvrageDAO.findReservations();
    }

    public void updateReservations(Ouvrage ouvrage) throws SQLException {
        ouvrageDAO.updateReservations(ouvrage);
    }

    public void reserver(User user) throws SQLException {
        System.lineSeparator();
        System.out.println("Menu emprunt :");
        System.lineSeparator();
        System.out.println("Saisir le titre de l'oeuvre :");
        System.lineSeparator();
        String entry = sc.nextLine().trim();
        System.lineSeparator();
        Oeuvre oeuvre = oeuvreDAO.findByTitle(entry);
        if (oeuvre == null) {
            System.out.println("Oeuvre introuvable");
            return;
        }
        if (oeuvre.getNbreDispo() == 0) {
            System.out.println("Oeuvre n'est plus disponible, souhaitez vous tout de même continuez ?"
                    + System.lineSeparator() + "L'ouvrage ne sera empruntable que lorsque le médiathécaire aura récupéré un ouvrage de l'oeuvre..."
                    + System.lineSeparator() + "         (\"o\" pour continuer malgré tout)");
            System.lineSeparator();
            entry = sc.nextLine().trim();
            System.lineSeparator();
            if ("o".equalsIgnoreCase(entry)) {
                System.out.println("Annulation en cours...");
                System.lineSeparator();
                return;
            }
        }
        Ouvrage ouvrage = ouvrageDAO.findByIdOeuvre(oeuvre.getId());

        if (user.getSolde() <= 0 || user.getSolde() < ouvrage.getPrix()) {
            System.out.println("réservation impossible, veuillez recharger votre solde ");
            System.out.println("entrée pour continuer ...");
            sc.nextLine();
        } else {
            user.setSolde(user.getSolde() - ouvrage.getPrix());
            authenticateController.updateSoldeReservation(user);

            ouvrageDAO.reserver(user, ouvrage);
            System.lineSeparator();
            System.out.println("Réservation de l'oeuvre pris en compte.");
            System.lineSeparator();
        }
    }

    public void afficherReservationsUtilisateur(User user) throws SQLException {
        System.lineSeparator();
        System.out.println("Menu affichage des réservations :");
        System.lineSeparator();
        ouvrageDAO.afficherReservationsUtilisateur(user);
        System.lineSeparator();
    }

    public void annulerReservation(User user) throws SQLException {
        System.lineSeparator();
        System.out.println("Menu annulation de réservation :");
        System.lineSeparator();
        System.out.println("Saisir le titre de l'oeuvre dont vous souhaitez annuler la reservation :");
        System.lineSeparator();
        String entry = sc.nextLine().trim();
        System.lineSeparator();
        Oeuvre oeuvre = oeuvreDAO.findByTitle(entry);
        System.out.println("Êtes vous sûr de vouloir annuler la réservation ? " + System.lineSeparator() + "        (\"o\" pour procéder à l'annulation) :");
        System.lineSeparator();
        if ("o".equalsIgnoreCase(entry)) {
            System.out.println("Annulation de la manoeuvre en cours...");
            return;
        }
        Ouvrage ouvrage = ouvrageDAO.findByIdOeuvre(oeuvre.getId());
        ouvrageDAO.annulerReservation(user, ouvrage);
        System.lineSeparator();
    }
}
