package fr.afpa.service.implementation;

import fr.afpa.dao.implementation.OeuvreDAOIMP;
import fr.afpa.dao.implementation.OuvrageDAOIMP;
import fr.afpa.model.Oeuvre;
import fr.afpa.model.User;
import fr.afpa.service.IService;
import fr.afpa.utilitary.ClassUtilitary;
import fr.afpa.utilitary.ConstantUtilitary;

import java.sql.Date;
import java.sql.SQLException;
import java.text.ParseException;

import static fr.afpa.view.implementation.AppViewIMP.sc;

//À tester ToDo

public class OeuvreServiceIMP implements IService<Oeuvre> {
    private static OeuvreDAOIMP oeuvreDAO = OeuvreDAOIMP.getInstance();
    private static final ClassUtilitary classUtilitary = ClassUtilitary.getInstance();
    private static OeuvreServiceIMP instance;

    public static OeuvreServiceIMP getInstance() {
        if (instance == null) {
            instance = new OeuvreServiceIMP();
        }
        return instance;
    }

    @Override
    public Oeuvre findById() throws SQLException {
        System.out.println("Recherche d'une Oeuvre par Id :");
        System.lineSeparator();
        System.out.println("Saisir l'id de l'oeuvre que vous souhaitez consulter :");
        System.lineSeparator();
        String entry = sc.nextLine().trim();
        Oeuvre oeuvre = oeuvreDAO.findById(Long.parseLong(entry));
        return oeuvre;
    }

    @Override
    public void showAll() throws SQLException {
        System.lineSeparator();
        System.out.println("Affichage de toutes les oeuvres :");
        System.lineSeparator();
        System.out.println("    *****   *****   *****");
        System.lineSeparator();
        oeuvreDAO.findAll();
        System.lineSeparator();
        System.out.println("    *****   *****   *****");
        System.lineSeparator();
    }

    @Override
    public boolean exist(long id) throws SQLException {
        return oeuvreDAO.exist(id);
    }

    @Override
    public void enregistrer() throws ParseException, SQLException {
        System.lineSeparator();
        System.out.println("Enregistrement de l'oeuvre :");
        System.lineSeparator();
        Oeuvre oeuvre = new Oeuvre();
        do{
            oeuvre.setId(classUtilitary.generateId());
        }while(oeuvreDAO.exist(oeuvre.getId()));
        System.out.println("Saisir le titre de l'oeuvre :");
        System.lineSeparator();
        String entry = sc.nextLine().trim();
        oeuvre.setTitre(entry);
        System.lineSeparator();
        System.out.println("Saisir le nombre d'oeuvre dispo :");
        System.lineSeparator();
        entry = sc.nextLine().trim();
        oeuvre.setNbreDispo(Integer.parseInt(entry));
        System.lineSeparator();
        System.out.println("Saisir le genre d'oeuvre (Livre/Film/Disque) :");
        System.lineSeparator();
        entry = sc.nextLine().trim();
        oeuvre.setGenre(entry);
        System.lineSeparator();
        System.out.println("Saisir la date de parution (format : yyyy-MM-dd) :");
        System.lineSeparator();
        entry = sc.nextLine().trim();
        Date date = Date.valueOf(entry);
        oeuvre.setAnneeParution(date);
        oeuvreDAO.create(oeuvre);
        System.lineSeparator();
        System.out.println("Enregistrement terminé, l'oeuvre a bien été enregistrée !");
    }

    @Override
    public void supprimer() throws SQLException {
        System.lineSeparator();
        System.out.println("Menu supprimer :");
        System.lineSeparator();
        System.out.println("Saisir le titre de l'oeuvre :");
        System.lineSeparator();
        String entry = sc.nextLine().trim();
        System.lineSeparator();
        Oeuvre oeuvre = oeuvreDAO.findByTitle(entry);
        if (oeuvre == null) {
            System.out.println("Oeuvre introuvable");
            return;
        }
        System.out.println("Êtes vous sûr de vouloir supprimer l'oeuvre " + oeuvre.toString() + " ? (pressez la touche o pour confirmer)");
        System.lineSeparator();
        entry = sc.nextLine().trim();
        System.lineSeparator();
        if (!entry.equalsIgnoreCase("o")) {
            System.out.println("Annulation de la suppression.");
            return;
        }
        System.out.println("Suppression en cours...");
        System.lineSeparator();
        oeuvreDAO.delete(oeuvre);
        System.out.println("L'oeuvre " + oeuvre.toString() + " a bien été supprimée.");
        System.lineSeparator();
    }
}
